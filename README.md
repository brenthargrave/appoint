# README

## Dev quickstart (with MacOS)

Install [homebrew](https://brew.sh/)

Execute bootstrap script:

```
./scripts/bootstrap.sh
```

Run the dev server:

```
cp .env.example .env
heroku local -f Procfile.dev
open http://localhost:5000
```

Deployed to Heroku at:

https://appoint-8256a4a1b26c.herokuapp.com/
