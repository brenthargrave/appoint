#!/bin/bash
set -x
set -u
set -e
set -v

brew bundle -v --file=.brewfile --no-upgrade --no-lock

asdf plugin-add python nodejs
asdf install
