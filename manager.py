from datetime import datetime, timedelta
import logging
log = logging.getLogger("logger")

class Manager:
    def __init__(self):
        self.appointments = {}

    def add(self, patient, doctor, start_iso):
        if patient is None:
            raise ValueError("patient is required")
        if doctor is None:
            raise ValueError("doctor is required")
        if start_iso is None:
            raise ValueError("slot is required")
        appts_by_doctor = self.appointments.get(doctor)
        if appts_by_doctor is not None and start_iso in appts_by_doctor:
            raise Exception("appointment taken")
        appt = {
            "patient": patient,
            "doctor": doctor,
            "start_iso": start_iso,
        }
        if doctor not in self.appointments:
            self.appointments[doctor] = {}
        self.appointments[doctor][start_iso] = appt
        return appt

    def doctors(self):
        result = list(self.appointments.keys())
        result.sort()
        return result

    def schedule_for(self, doctor):
        appts_for_doctor = self.appointments.get(doctor) or {}
        first_day = datetime.today() + timedelta(days=1)
        dates = [first_day + timedelta(days=x) for x in range(14)]
        hours = range(14, 23, 1) # NOTE: 9am to 5pm
        result = []
        for date in dates:
            for hour in hours:
                start = date.replace(hour=hour, minute=0, second=0, microsecond=0)
                # NOTE: https://stackoverflow.com/a/42777551
                start_iso = start.strftime('%Y-%m-%dT%H:%M:%SZ')
                booked = True if start_iso in appts_for_doctor else False
                slot = {
                    "start_iso": start_iso,
                    "booked": booked,
                }
                result.append(slot)
        return result

    def get_appointments(self):
        result = []
        for doctor in self.appointments.keys():
            appts = self.appointments[doctor]
            print(appts)
            result += list(appts.values())
        return result
