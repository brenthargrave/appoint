import { Button, Container, HStack, Heading, Spacer, Text, VStack } from '@chakra-ui/react'
import * as React from 'react'
import { useState } from "react";
import {
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  TableContainer,
} from '@chakra-ui/react'
import { useAsync } from 'react-use';
import { parseISO } from "date-fns";

type Appointment = {
  doctor: string
  patient: string
  start_iso: string
}
interface AppointmentFormatted extends Appointment {
  start: string
}

export const List = () => {

  const [appts, setAppts] = useState<AppointmentFormatted[]>([])
  const apptsRequest = useAsync(async (_) => {
    const response = await fetch("/api/appointments")
    const appts: Appointment[] = await response.json()
    const formatted: AppointmentFormatted[] = appts.map((appt) => {
      return {
        ...appt,
        start: parseISO(appt.start_iso).toLocaleString()
      }
    })
    setAppts(formatted)

  }, [])

  return (
    <VStack align={"start"} spacing={4} pt={12}>
      <Heading size="md">Booked appointments</Heading>
      <TableContainer>
        <Table variant={"simple"}>
          <Thead>
            <Tr>
              <Th>Doctor</Th>
              <Th>Patient</Th>
              <Th>Date / Time</Th>
            </Tr>
          </Thead>
          <Tbody>
            {appts.map((appt) => {
              return (
                <Tr key={appt.start_iso}>
                  <Td>{appt.doctor}</Td>
                  <Td>{appt.patient}</Td>
                  <Td>{appt.start}</Td>
                </Tr>
              )
            })}
          </Tbody>
        </Table>
      </TableContainer>
    </VStack>
  )
}
