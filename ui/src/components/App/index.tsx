import { Button, Container, HStack, Heading, Spacer, Text, VStack } from '@chakra-ui/react'
import * as React from 'react'
import { useRoute, routes } from '~/router'
import { Create } from "./Create";
import { List } from "./List";

export const App = () => {
  const route = useRoute()

  return (
    <HStack align={"start"}>
      {/* Nav */}
      <VStack align={"center"} padding={4}>
        <Heading size={"lg"}>Appoint.</Heading>
        <Spacer />
        <VStack width={"100%"} alignItems={"stretch"}>
          <Button isActive={route.name == "make"} onClick={() => routes.make().push()}>Make appointment</Button>
          <Button isActive={route.name == "list"} onClick={() => routes.list().push()}>List appointments</Button>
        </VStack>
      </VStack>
      {/* Main */}
      <VStack padding={4}>
        <Container>
          {route.name == "list" && <List />}
          {route.name == "make" && <Create />}
          {route.name == false && <Text>Not Found</Text>}
        </Container>
      </VStack>
    </HStack>
  )

}
