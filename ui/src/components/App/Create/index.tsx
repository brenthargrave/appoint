import {
  Button,
  FormControl,
  FormLabel,
  Heading,
  Input,
  Select,
  VStack
} from "@chakra-ui/react";
import { CreatableSelect } from 'chakra-react-select';
import { parseJSON, startOfDay, startOfToday } from "date-fns";
import * as React from 'react';
import { useState } from "react";
import { useAsync } from "react-use";
import { groupBy, last } from "remeda";
import { DatePicker } from "./DatePicker";
import { routes } from "~/router";

type Doc = string
interface Slot {
  start_iso: string
  booked: boolean
}
type Schedule = Slot[]
interface SlotWithDay extends Slot {
  start: Date
  day: Date
}

export const Create = () => {
  // Patient name
  const [patient, setPatient] = useState<string>("")

  // Doctor/Specialist selection
  // Available slots are filtered by selected doc; we allow addition of
  // new docs from within the form.
  const [docOptions, setDocOptions] = useState<Doc[]>([])
  const [selectedDoc, setSelectedDoc] = useState<Doc | null>(null)
  const addDoc = (doc: Doc) => {
    setDocOptions([...docOptions, doc])
  }
  const docRequest = useAsync(async (_) => {
    const response = await fetch("/api/doctors")
    const docs = await response.json()
    console.info(docs)
    setDocOptions(docs)
    return docs
  }, [])


  // Date
  const [date, setDate] = useState<Date>(startOfToday())
  const [disableAfter, setDisableAfter] = useState<Date | undefined>(undefined)
  const [disableDates, setDisableDates] = useState<Date[]>([])

  // Time slot
  const [slotsByDate, setSlotsByDate] = useState<{ [day: string]: SlotWithDay[] }>({})
  const [selectedSlotISO, setSelectedSlotISO] = useState<string | null>(null)

  const scheduleRequest = useAsync(async () => {
    if (!!selectedDoc) {
      const doctor = selectedDoc
      const response = await fetch('/api/schedule?' + new URLSearchParams({ doctor }))
      console.info(response)
      const slots: Schedule = await response.json()
      console.info(slots)

      // TODO: disableDates = if all slots for date are booked

      // NOTE: parse start_iso into local datetime and "day" (startOfDay)
      const slotsWithDay: SlotWithDay[] = slots.map((slot) => {
        const start = parseJSON(slot.start_iso)
        const day = startOfDay(start)
        return {
          ...slot,
          start,
          day
        }
      })
      console.info(slotsWithDay)
      // NOTE: group slots by day for calendar presentation
      const slotsByDate = groupBy(slotsWithDay, (slot: SlotWithDay) => slot.day.toISOString())
      console.info(slotsByDate)
      setSlotsByDate(slotsByDate)
      setSelectedSlotISO(null) // clear on doc change

      const lastSlotDate = last(slotsWithDay)?.day
      setDisableAfter(lastSlotDate)

    }
  }, [selectedDoc])

  const handleDateChange = (date: Date) => {
    setDate(date)
    setSelectedSlotISO(null) // clear on date change
  }

  const selectedDateISO = date.toISOString()
  const selectedDateSlots: SlotWithDay[] = slotsByDate[selectedDateISO] ?? []

  const [bookLoading, setBookLoading] = useState<boolean>(false)
  const book = async () => {
    setBookLoading(true)
    console.info(patient)
    const body = {
      patient,
      doctor: selectedDoc,
      start_iso: selectedSlotISO
    }
    console.info(body)
    const response = await fetch('/api/book',
      {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(body)
      }
    )
    const result = await response.json();
    console.info(result)
    setBookLoading(false)

    routes.list().push()
  }


  return (
    <VStack align={"start"} spacing={4} pt={12}>
      <Heading size="md">Make an appointment</Heading>
      <form>
        <VStack spacing={4}>
          <FormControl>
            <FormLabel>Patient name</FormLabel>
            <Input name="patient" onChange={(e => setPatient(e.target.value))} />
          </FormControl>
          <FormControl id="doctor" isDisabled={patient.length < 3}>
            <FormLabel>Doctor / Specialist name</FormLabel>
            <CreatableSelect
              isLoading={docRequest.loading}
              placeholder="Select or type name to add..."
              noOptionsMessage={(inputValue) => "Add a doctor by typing their name."}
              formatCreateLabel={(inputValue) => `Add "${inputValue}"`}
              createOptionPosition='first'
              onChange={(option) => setSelectedDoc(option!.value)}
              onCreateOption={(inputValue: string) => {
                console.info(inputValue)
                addDoc(inputValue)
                setSelectedDoc(inputValue)
              }}
              options={docOptions.map((doc) => ({ value: doc, label: doc }))}
              value={selectedDoc ? { value: selectedDoc, label: selectedDoc } : null}
            />
          </FormControl>
          <FormControl >
            <FormLabel>Appointment date</FormLabel>
            <DatePicker
              isDisabled={!selectedDoc || scheduleRequest.loading}
              selectedDate={date}
              setDate={handleDateChange}
              disableDates={disableDates}
              disableAfter={disableAfter}
            />
            {/* <div>{date.toDateString()}</div> */}
          </FormControl>
          <FormControl
            isDisabled={!selectedDoc || scheduleRequest.loading}
          >
            <FormLabel>Appointment time</FormLabel>
            <Select
              placeholder='Select time'
              onChange={(e) => {
                // NOTE: eg, "2024-04-22T16:00:00Z"
                const slotStartISO = e.target.value
                console.info(slotStartISO)
                if (!!slotStartISO) {
                  setSelectedSlotISO(slotStartISO)
                }
              }}
            >
              {selectedDateSlots.map((slot: SlotWithDay) => {
                return (
                  <option
                    key={slot.start_iso}
                    value={slot.start_iso}
                    disabled={slot.booked}>
                    {slot.start.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' })}
                  </option>
                )
              })}
            </Select>
          </FormControl>

          <Button
            isDisabled={!selectedSlotISO}
            isLoading={bookLoading}
            alignSelf={"start"}
            onClick={async () => {
              await book()
            }}
          >
            Book
          </Button>

        </VStack>
      </form >
    </VStack>
  )
}
