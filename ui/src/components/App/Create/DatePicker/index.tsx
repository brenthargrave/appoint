import { ChakraProvider } from '@chakra-ui/react';
import {
  Calendar,
  CalendarControls,
  CalendarDate,
  CalendarDays,
  CalendarDefaultTheme,
  CalendarMonth,
  CalendarMonthName,
  CalendarMonths,
  CalendarNextButton,
  CalendarPrevButton,
  CalendarWeek
} from '@uselessdev/datepicker';
import * as React from 'react';
import { subDays } from "date-fns";

export interface Props {
  isDisabled: boolean
  selectedDate: Date
  setDate: (date: Date) => void
  disableDates: Date[]
  disableAfter: Date | undefined
}

export const DatePicker = (props: Props) => {
  const { isDisabled = true, selectedDate, setDate, disableDates, disableAfter } = props
  const handleSelectDate = (date: CalendarDate) => {
    console.log(date)
    setDate(new Date(date)) // coerce number to date
  }

  return (
    <div
      // NOTE: datepicker doesn't support disabled state natively
      // https://stackoverflow.com/questions/51259528/how-to-totally-disable-a-react-component
      style={isDisabled ? { pointerEvents: "none", opacity: "0.4" } : {}}
    >
      <ChakraProvider
        theme={CalendarDefaultTheme}
      >
        <Calendar
          value={{ start: selectedDate }}
          // @ts-ignore
          onSelectDate={handleSelectDate}
          highlightToday
          singleDateSelection
          disablePastDates={subDays(new Date(), 1)}
          disableDates={disableDates}
          disableFutureDates={disableAfter}
        >
          <CalendarControls>
            <CalendarPrevButton />
            <CalendarNextButton />
          </CalendarControls>

          <CalendarMonths>
            <CalendarMonth>
              <CalendarMonthName />
              <CalendarWeek />
              <CalendarDays />
            </CalendarMonth>
          </CalendarMonths>
        </Calendar>
      </ChakraProvider>
    </div>

  )
}
