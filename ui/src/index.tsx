import { ChakraProvider } from '@chakra-ui/react';
import * as React from 'react';
import { createRoot } from 'react-dom/client';
import { RouteProvider } from '~/router';

import { App } from "~/components/App";

const container = document.getElementById('app');
const root = createRoot(container!)
root.render(
  <ChakraProvider>
    <RouteProvider>
      <App />
    </RouteProvider>
  </ChakraProvider>
);
