import { createRouter, defineRoute } from "type-route";

export const { RouteProvider, useRoute, routes } = createRouter({
  list: defineRoute(["/list", "/"]),
  make: defineRoute(
    {},
    () => "/make"
  ),
});
