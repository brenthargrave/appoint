import { defineConfig, splitVendorChunkPlugin } from "vite"
import react from '@vitejs/plugin-react'


export default defineConfig({
  plugins: [
    react(),
    splitVendorChunkPlugin(),
  ],
  server: {
    port: 3000,
    strictPort: true,
  },
  resolve: {
    alias: {
      '~': '/src',
    }
  },
  build: {
    sourcemap: true,
    outDir: "../public",
    emptyOutDir: true,
    manifest: true,
    rollupOptions: {
      input: "/src/index.tsx",
    },
  },
})
