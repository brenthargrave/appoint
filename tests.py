import unittest
from manager import Manager

class TestManager(unittest.TestCase):

    def test_add(self):
        manager = Manager()
        patient = "Hargrave"
        doc = "House"
        start_iso = "2024-04-22T09:00:00Z"
        # prevent nil values
        with self.assertRaises(ValueError):
            manager.add(None, doc, start_iso)
        with self.assertRaises(ValueError):
            manager.add(patient, None, start_iso)
        with self.assertRaises(ValueError):
            manager.add(patient, doc, None)
        # prevent dupe appointments by doctor
        manager.add(patient, doc, start_iso)
        with self.assertRaises(Exception) as context:
            manager.add(patient, doc, start_iso)
        # verify dupe time for diff docs
        manager.add(patient, "Moreau", start_iso)
        manager.add(patient, doc, "2024-04-23T09:00:00Z")

    def test_doctors(self):
        manager = Manager()
        patient = "Hargrave"
        start_iso = "2024-04-22T09:00:00Z"
        manager.add(patient, "Moreau", start_iso)
        manager.add(patient, "House", start_iso)
        self.assertEqual(manager.doctors(), ["House", "Moreau"])

    # TODO: test schedule generation
    def test_schedule(self):
        manager = Manager()
        schedule = manager.schedule_for("House")

if __name__ == '__main__':
    unittest.main()
