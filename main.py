from flask import Flask, render_template, request
from assets_blueprint import assets_blueprint
from dateutil import parser
import logging
log = logging.getLogger("logger")
from manager import Manager

app = Flask(
    __name__,
    static_url_path="/assets/",
    static_folder="public",
    template_folder="templates",
)

app.register_blueprint(assets_blueprint)

manager = Manager()

@app.route("/api/doctors", methods=["GET"])
def doctors():
    return manager.doctors()

@app.route("/api/schedule", methods=["GET"])
def schedule():
    doctor = request.args.get("doctor")
    return manager.schedule_for(doctor)

@app.route('/api/book', methods=['POST'])
def book():
    data = request.get_json()
    patient = data.get("patient")
    doctor = data.get("doctor")
    start_iso = data.get("start_iso")
    appt = manager.add(patient, doctor, start_iso)
    return appt

@app.route('/api/appointments', methods=['GET'])
def appointments():
    result = manager.get_appointments()
    return result

@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def catch_all(path):
    return render_template("index.html")
